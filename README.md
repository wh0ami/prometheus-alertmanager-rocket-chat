# Prometheus/Alertmanager integration for Rocket.Chat

Do you ever wanted to integrate your [Prometheus](https://prometheus.io/) monitoring into
your [Rocket.Chat](https://rocket.chat/) instance? Here's the solution - let's go!

## Implementation

1. In your Rocket.Chat instance, go to "Administration -> Integrations -> Incoming" and click on "New".
2. Configuration:
    1. Set the switch "Enabled" to enable
    2. Set the "Name" of the integration (e.g. "Prometheus Alertmanager")
    3. Enter the channel, where the messages should be sent to in "Post to Channel"
    4. Set the user that should be used to send the messages in "Post as"
    5. Set Emoji to `:rotating_light:`
    6. Set the switch "Script Enabled" to enable
    7. Paste the content of the file `integration.js` in the "Script" box
3. Click "Save" at the bottom of the page
4. The "Webhook URL" will show a value now - select and copy it to your clipboard
5. Go into your Alertmanager config file and configure it using the copied URL (example can be found below)
6. Restart Alertmanager
7. If not already done, configure some rules in your Prometheus instance and restart it (example can be found below)
8. You are already done - now, provoke an alarm to test the integration

## Configuration examples

### Alertmanager

```yaml
global:
  resolve_timeout: 5m

route:
  group_by: [ 'alertname' ]
  group_wait: 10s
  group_interval: 10s
  repeat_interval: 3h
  receiver: 'MonitoringTeam'

receivers:
  - name: 'MonitoringTeam'
    webhook_configs:
      - send_resolved: true
        url: 'https://rocketchat.mydomain.de/hooks/xxx/yyy'

inhibit_rules:
  - source_match:
      severity: 'critical'
    target_match:
      severity: 'warning'
    equal: [ 'alertname', 'dev', 'instance' ]
```

### Prometheus (only rules)

```yaml
groups:
  - name: /caddy.rules.yml
    rules:

      ### Requests per second ###
      - alert: TooManyRequestsPerSecond
        expr: sum by (instance) (rate(caddy_http_requests_total{job="caddy"}[5m])) > 150
        for: 15m
        labels:
          severity: warning
        annotations:
          summary: "Too many requests per second on {{ $labels.instance }}"
          description: "There are more than 150 requests per second in the last 15 minutes on {{ $labels.instance }}\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"

      ### Too many unusual response codes per second ###
      - alert: TooManyUnusualResponseCodesPerSecond
        expr: sum by (instance, code) (irate(caddy_http_request_duration_seconds_count{job="caddy", code !~ "^(200|304)$"}[5m])) > 5
        for: 15m
        labels:
          severity: warning
        annotations:
          summary: "Too many requests with response code {{ $labels.code }} per second on {{ $labels.instance }})"
          description: "There are more than 5 requests per second in the last 15 minutes on {{ $labels.instance }}, that were answered with HTTP response code {{ $labels.code }},\n  VALUE = {{ $value }}\n  LABELS: {{ $labels }}"
```

## Screenshots

Please note that I only passed some bullshit data here. This is just to imagine how the result will/should look like.

![screenshots/example_1.png](screenshots/example_1.png)
